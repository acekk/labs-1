﻿using System;
using System.Collections.Generic;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public class Program
    {
        public IList<IMundurowy> Mundurowi()
        {
            IList<IMundurowy> mundurowy = new List<IMundurowy>();

            mundurowy.Add(new Policjant());
            mundurowy.Add(new Strazak());
            mundurowy.Add(new Policjant());
            mundurowy.Add(new Robot());

            mundurowy[0].Wkrocz_do_akcji();
            mundurowy[1].Wkrocz_do_akcji();
            mundurowy[2].Wkrocz_do_akcji();
            mundurowy[3].Wkrocz_do_akcji();

            return mundurowy;
        }

        public void WezMundurowego(List<IMundurowy> mundurowy)
        {
            mundurowy.ForEach(c => c.Wkrocz_do_akcji());
        }

        public static void Main() { }
    }
}
