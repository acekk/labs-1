﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    public class Robot : IRobot, IPolicjant
    {
        public string Strzelaj()
        {
            return "tratatatata";
        }

        public string Wkrocz_do_akcji()
        {
            return "robot rusza do akcji";
        }

        public string Polegl()
        {
            return "robot polegl";
        }

        public string Niszcz()
        {
            return "łubudu";
        }

        string IMundurowy.Polegl()
        {
            return "robot polegl jak mundurowy";
        }

        string IRobot.Polegl()
        {
            return "robot polegl jak on sam";
        }

        string IPolicjant.Polegl()
        {
            return "robot spalowany";
        }
    }
}
