﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    public interface IRobot : IMundurowy
    {
        new string Wkrocz_do_akcji();
        new string Polegl();

        string Niszcz();
        string Strzelaj();
    }
}
