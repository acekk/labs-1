﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(IMundurowy);

        public static Type ISub1 = typeof(IPolicjant);
        public static Type Impl1 = typeof(Policjant);

        public static Type ISub2 = typeof(IStrazak);
        public static Type Impl2 = typeof(Strazak);


        public static string baseMethod = "Wkrocz_do_akcji";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Wkrocz_do_akcji";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Wkrocz_do_akcji";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Mundurowi";
        public static string collectionConsumerMethod = "WezMundurowego";

        #endregion

        #region P3

        public static Type IOther = typeof(IRobot);
        public static Type Impl3 = typeof(Robot);

        public static string otherCommonMethod = "Polegl";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
